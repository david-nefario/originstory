import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Storage } from '@ionic/storage';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

const RESOURCE_URL = `<resource-location-here>`;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private jwt: String = null;

  constructor(private http: HttpClient, public storage: Storage) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(`Client-side Error: ${ error.error.message }`)
    } else {
      console.error(`Back-end Error: ${ error.status } / ${ error.error }`);
    }

    return throwError(`Something went finnicky - look at the console`);
  }

  private extractData(res: Response) : any {
    let body = res;
    return body || {};
  }

  loadJwt(jwt: string) {
    this.jwt = jwt;
    this.storage.set(`jwt`, jwt);
  }

  private getContentJsonHeaders() : HttpHeaders {
    return new HttpHeaders({'Content-Type': `application/json`});
  }

  private getAuthorizedContentJsonHeaders() : HttpHeaders {
    return this.getContentJsonHeaders().
        append(`Authorization`, `Bearer ${ this.jwt }`)
  }

  // This template assumes a basic register call, requiring an email
  // and password, POSTed via JSON. Extend as necessary.
  register(email: string, password: string) : Observable<any> {
    return this.http.post(`${ RESOURCE_URL }<register-path>`,
        JSON.stringify({
          email: email,
          password: password,
        }), {
          headers: this.getContentJsonHeaders(),
        })
        .pipe(map(this.extractData), catchError(this.handleError));
  }

  // This template assumes a basic login call, requiring an email and
  // a password, POSTed via JSON. Extend as necessary.
  // If it receives a body with a `token` var, assigns its value to the JWT
  login(email: string, password: string) : Observable<any> {
    let subscription = this.http.post(`${ api_v1 }<login-path>`,
        JSON.stringify({
          email: email,
          password: password
        }), {
          headers: this.getContentJsonHeaders()
        })
        .pipe(map(this.extractData), catchError(this.handleError));

    subscription.subscribe((body) => {
      this.loadJwt(body.token);
    });

    return subscription;
  }

  isLoggedIn() : boolean {
    return this.jwt !== null;
  }

  loginCheck() {
    if (!this.isLoggedIn()) {
      throw new Error("not logged in");
    }
  }
}
